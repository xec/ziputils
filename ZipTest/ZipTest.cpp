// ZipTest.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "zip.h"
#include "unzip.h"

int _tmain(int argc, _TCHAR* argv[])
{
    HZIP zip = CreateZip("压缩包.zip", "123456");
    ZipAdd(zip, "中文目录/中文文件名", "abc", 3);

    CloseZip(zip);

    zip = OpenZip("压缩包.zip", "123456");


    ZIPENTRY ze = { 0 };
    GetZipItem(zip, -1, &ze);
    int numitems = ze.index;
    for (int i = 0; i < numitems; i++) {
        char buffer[1024] = { 0 };
        GetZipItem(zip, i, &ze);
        UnzipItem(zip, i, buffer, 1024);

        printf("data = %s\n", buffer);
    }
    CloseZip(zip);
    return 0;
}

